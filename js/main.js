function ladujDane() {
    var xml = new XMLHttpRequest();
    
    xml.onreadystatechange = function() {
        if (this.readyState === 4) { //XMLHttpRequest.DONE
                if (this.status === 200) {
                    document.querySelector('#content').innerHTML = this.responseText;
                    //tutaj operacje na dokumencie
                }
        }
    }
    
    xml.open("GET", '/content/plik.txt', true);
    xml.send();
}

function test() {
    {
        var a = 15;
    }
    console.log(a);
    {
        b = -5; //<= window.b = -5
    }
    console.log(b);
    
}
